def soma_lista(lista):
    if(len(lista) > 0):
        soma = lista[0]
        lista = lista[1:]
        return soma_lista(lista) + soma
    return 0

def main():
    lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print(soma_lista(lista))


main()