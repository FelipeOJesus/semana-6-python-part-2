def fibonacci(n):
    if n > 1:
        return fibonacci(n -1) + fibonacci(n-2)
    return n
    
def main():
    print(fibonacci(4))
    # deve devolver => 3
    print(fibonacci(2))
    # deve devolver => 1  


main()