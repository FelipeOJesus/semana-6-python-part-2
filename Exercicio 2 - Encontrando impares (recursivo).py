listaFinal = []

def encontra_impares(lista):
    
    if(len(lista) > 0):
        if(lista[0] % 2 == 1):
            listaFinal.extend([lista[0]])
        encontra_impares(lista[1:]);    
    return listaFinal

def main():
    lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    print(encontra_impares(lista))


main()